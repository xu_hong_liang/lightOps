<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/context/mytags.jsp"%>
<!-- <h4>分类标题</h4> -->
	    <div class="row">
	      <div class="col-md-12 layout-header">
	        <button id="addBtn_SqlTask" type="button" class="btn btn-default">添加</button>
	        <button id="delBtn_SqlTask" type="button" class="btn btn-default">删除</button>
	       
	        <script type="text/javascript"> 
			$('#addBtn_SqlTask').bind('click', function(){   
		 		 var tr =  $("#add_sqlTask_table_template>tr").clone();
			 	 $("#add_sqlTask_table").append(tr);
			 	 resetTrNum('add_sqlTask_table');
			 	 return false;
		    });  
			$('#delBtn_SqlTask').bind('click', function(){   
		       $("#add_sqlTask_table").find("input[name$='ck']:checked").parent().parent().remove();   
		        resetTrNum('add_sqlTask_table');
		        return false;
		    });
		    $(document).ready(function(){
		    	if(location.href.indexOf("load=detail")!=-1){
					$(":input").attr("disabled","true");
					$(".datagrid-toolbar").hide();
				}
		    	resetTrNum('add_sqlTask_table');
		    });
		   </script>
	      </div>
	    </div>
	    
	    
<div style="margin: 0; background-color: white;overflow: auto;">    
	    <!-- Table -->
      <table id="sqlTask_table" class="table table-bordered table-hover" style="margin-bottom: 0;">
		<thead>
	      <tr>
	        <th style="white-space:nowrap;width:50px;">操作</th>
	        <th style="width:40px;">序号</th>
			<th>
			数据库ID
			</th>
			<th>
			SQL_ID
			</th>
			<th>
			执行状态
			</th>
	      </tr>
	    </thead>
        
	<tbody id="add_sqlTask_table">	
	<c:if test="${fn:length(sqlTaskList)  <= 0 }">
	<tr>
		<td><input style="width:20px;" type="checkbox" name="ck"/></td>
			<input name="sqlTaskList[0].id" type="hidden"/>
			<input name="sqlTaskList[0].createName" type="hidden"/>
			<input name="sqlTaskList[0].createBy" type="hidden"/>
			<input name="sqlTaskList[0].createDate" type="hidden"/>
			<input name="sqlTaskList[0].updateName" type="hidden"/>
			<input name="sqlTaskList[0].updateBy" type="hidden"/>
			<input name="sqlTaskList[0].updateDate" type="hidden"/>
			<input name="sqlTaskList[0].sysOrgCode" type="hidden"/>
			<input name="sqlTaskList[0].sysCompanyCode" type="hidden"/>
			<input name="sqlTaskList[0].bpmStatus" type="hidden"/>
		<th scope="row"><div name="xh"></div></th>
		 
		<td>
			<input name="sqlTaskList[0].dbId"  style="width:150px" maxlength="36" type="text" class="form-control"   datatype="*"  ignore="checked" />
		</td>
		 
		<td>
			<input name="sqlTaskList[0].sqlId"  style="width:150px" maxlength="36" type="text" class="form-control"   datatype="*"  ignore="checked" />
		</td>
		 
		<td>
			<input name="sqlTaskList[0].executeFlag"  style="width:150px" maxlength="1" type="text" class="form-control"   datatype="n"  ignore="checked" />
		</td>
   	</tr>
	</c:if>
	<c:if test="${fn:length(sqlTaskList)  > 0 }">
	<c:forEach items="${sqlTaskList}" var="poVal" varStatus="stuts">
	<tr>
		<td><input style="width:20px;" type="checkbox" name="ck"/></td>
			<input name="sqlTaskList[${stuts.index }].id" type="hidden" value="${poVal.id }"/>
			<input name="sqlTaskList[${stuts.index }].createName" type="hidden" value="${poVal.createName }"/>
			<input name="sqlTaskList[${stuts.index }].createBy" type="hidden" value="${poVal.createBy }"/>
			<input name="sqlTaskList[${stuts.index }].createDate" type="hidden" value="${poVal.createDate }"/>
			<input name="sqlTaskList[${stuts.index }].updateName" type="hidden" value="${poVal.updateName }"/>
			<input name="sqlTaskList[${stuts.index }].updateBy" type="hidden" value="${poVal.updateBy }"/>
			<input name="sqlTaskList[${stuts.index }].updateDate" type="hidden" value="${poVal.updateDate }"/>
			<input name="sqlTaskList[${stuts.index }].sysOrgCode" type="hidden" value="${poVal.sysOrgCode }"/>
			<input name="sqlTaskList[${stuts.index }].sysCompanyCode" type="hidden" value="${poVal.sysCompanyCode }"/>
			<input name="sqlTaskList[${stuts.index }].bpmStatus" type="hidden" value="${poVal.bpmStatus }"/>
		<th scope="row"><div name="xh">${stuts.index+1 }</div></th>
				
		  <td>
			<input name="sqlTaskList[${stuts.index}].dbId" value = "${poVal.dbId}" style="width:150px" maxlength="32" type="text" class="form-control"   datatype="n"  ignore="checked" />
		  </td>
		  <td>
			<input name="sqlTaskList[${stuts.index}].sqlId" value = "${poVal.sqlId}" style="width:150px" maxlength="36" type="text" class="form-control"   datatype="*"  ignore="checked" />
		  </td>
		  <td>
			<input name="sqlTaskList[${stuts.index}].executeFlag" value = "${poVal.executeFlag}" style="width:150px" maxlength="1" type="text" class="form-control"   datatype="n"  ignore="checked" />
		  </td>
	</tr>
	</c:forEach>
	</c:if>	
	</tbody>
</table>
