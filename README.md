# lightOps（轻量级运维管理系统）

  
:heavy_exclamation_mark:  :heavy_exclamation_mark:  :heavy_exclamation_mark:
:heavy_exclamation_mark:  :heavy_exclamation_mark:  :heavy_exclamation_mark:     
:heavy_exclamation_mark:  :heavy_exclamation_mark:  :heavy_exclamation_mark:  **新版设计中**   
:heavy_exclamation_mark:  :heavy_exclamation_mark:  :heavy_exclamation_mark: **最近准备重新设计一套运维管理软件，目前还在设计中，设计思路可以看这里：https://blog.csdn.net/joshho/article/details/117564646** 

 **主页预览图：** 
![在这里插入图片描述](https://img-blog.csdnimg.cn/20210622171933244.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L2pvc2hobw==,size_16,color_FFFFFF,t_70)



# 需求背景
1. 在开发过程中修改数据库数据或表结构，但是在上线时需要将变动的sql脚本刷到N个表结构相同的库中，如何方便的更新过去？
2. 如果你只需要监控相关系统URL的状态，但是zabbix、nagios的安装又太复杂，怎么办？
3. 如果你想监控相关服务器的端口状态，又不想用zabbix、nagios怎么办？
4. 如果你平常开发时的sql更新脚本提交在svn，但是在上线时忘记了哪些执行过，哪些没执行过，怎么办？
5. 暂时只支持上述目的。

# 开发介绍
该项目是在快速开发框架Jeecg-3.7.8版本的基础上开发的，代码也比较容易维护，方便二次开发。
如果有什么建议，可以给我留言。
Jeecg社区地址：http://www.jeecg.org/


# 功能介绍
1. 一键将SQL脚本在N个数据库中执行。
2. 从SVN导入SQL内容，然后通过平台执行。
3. 监控端口状态（目前只是打开页面时更新状态）
4. 监控URL状态（目前只是打开页面时更新状态）

# 安装说明
1. 项目使用Tomcat8.5，JDK1.8，MySQL
2. 下载下来后，将项目转为Maven项目，然后修改resources目录下的dbconfig.properties中的数据库连接即可。
3. 建库建本：docs/jeecg-3.7.8-oc.sql

# 使用说明
批量更新SQL脚本使用说明：
1. 系统监控->多数据源管理，配置数据源。
2. 运维管理->数据库管理，配置数据库。（配置数据库时，“数据库标识”填写“多数据源管理”里面的“多数据源主键”）
3. SQL管理->添加SQL
4. SQL管理->执行任务，关联SQL需要执行的数据库，点击“执行”按钮即可。

# 系统截图：
## 1 登录界面
![1登录界面](https://images.gitee.com/uploads/images/2018/1117/191347_dd178469_367746.jpeg "1登录界面.jpg")


## 2 登录后界面
![2登录后界面](https://images.gitee.com/uploads/images/2018/1117/191455_7b502c62_367746.jpeg "2登录后界面.jpg")


## 3 执行SQL功能

“SQL脚本管理”-》“SQL管理”-》SQL列表页面：
![](https://images.gitee.com/uploads/images/2018/1121/214058_f08bd401_367746.jpeg "从SVN导入SQL功能.jpg")


### 3.1 配置目标数据库
![配置数据库界面](https://images.gitee.com/uploads/images/2018/1117/191646_ad8bbebc_367746.jpeg "6配置数据库界面.jpg")


### 3.2 添加SQL
#### 3.2.1 方式一：从SVN导入SQL
1） 配置SVN上的SQL脚本目录、SVN账号、SVN密码：
![](https://images.gitee.com/uploads/images/2018/1121/213735_f7ddb138_367746.jpeg "配置SVN账号密码.jpg")

2）“SQL列表”-》点击“从SVN导入SQL”，结果：
![](https://images.gitee.com/uploads/images/2018/1121/213922_566c2b8c_367746.jpeg "从SVN导入SQL-2.jpg")


#### 3.2.2 方式二：手工添加SQL（点击SQL列表右上方的+号）
![](https://images.gitee.com/uploads/images/2018/1121/215322_4edc348f_367746.jpeg "手动添加SQL页面.jpg")


### 3.3 执行SQL
“SQL管理”列表页面-》点击“执行选中SQL”（弹出3.1中配好的数据库）：
![](https://images.gitee.com/uploads/images/2018/1121/214301_aaa1ae4c_367746.jpeg "选择SQL执行目标数据库.jpg")

### 3.4 执行结果记录
![](https://images.gitee.com/uploads/images/2018/1121/215202_8e997c22_367746.jpeg "执行记录.jpg")


## 4 URL监控
![URL监控界面](https://images.gitee.com/uploads/images/2018/1117/191704_44edc00a_367746.jpeg "7URL监控界面.jpg")


## 5 端口监控
![端口监控界面](https://images.gitee.com/uploads/images/2018/1117/191726_16809d3f_367746.jpeg "8端口监控界面.jpg")


